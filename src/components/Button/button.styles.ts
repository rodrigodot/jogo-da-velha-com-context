import styled from 'styled-components';

export const StyledButton = styled.button`
  background-color: #800040;
  color: #fff;

  margin: 0.5em;
  font-size: 1em;
  padding: 0.6em 2em;
  border: 2px solid palevioletred;
  border-radius: 0.3em;
  cursor: pointer;
  &:hover {
    opacity: 0.5;
  }
  transition: background-color 4s;
  outline: none;
`;
