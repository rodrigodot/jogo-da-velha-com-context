import React, { FC, useContext } from 'react';

import {
  StyledBoardWraper,
  StyledPlayer,
  StyledBoardContaner,
  StyledWinner,
  StyledHitoryContaner,
} from './board.styles';

import { Square, Button, ListItem } from '../../components';
import { GameContext } from '../../contexts/Board.context';
import getWinner from '../../utils/getWinner';

export const Board: FC = () => {
  const {
    currentPlayer = 'X',
    squares = [],
    isWinner = '',
    setCurrentPlayer = () => {},
    reset = () => {},
    setIsWinner = () => {},
    history = [],
    setHistory = () => {},
  } = useContext(GameContext);

  const handleClick = (index: number): void => {
    const newSquares = squares;
    newSquares[index] = currentPlayer;

    const newHistory = history;
    const newOne = `Casa ${index + 1} - Jogador: ${currentPlayer}`;
    newHistory.push(newOne);

    const player = currentPlayer === 'X' ? 'O' : 'X';
    setCurrentPlayer(player);

    setHistory(newHistory);
    const winner = getWinner(newSquares);
    if (winner) setIsWinner(winner);
  };

  return (
    <StyledBoardWraper>
      <StyledPlayer>Próximo jogador: {currentPlayer}</StyledPlayer>
      <StyledWinner>
        {isWinner ? `Vencedor: Jogador ${isWinner}` : ''}
      </StyledWinner>

      <StyledBoardContaner>
        {!!squares?.length &&
          squares.map((square, index) => {
            return (
              <Square
                key={index}
                title={square}
                onClick={() => handleClick(index)}
                squareProps={{
                  disabled: !!square || !!isWinner,
                }}
              />
            );
          })}
      </StyledBoardContaner>
      <Button title="Resetar" onClick={() => reset()} />

      <StyledHitoryContaner>
        {!!history.length &&
          history.map((item, index) => {
            return (
              <ListItem
                key={index}
                index={`${index}º`}
                details={item}
              />
            );
          })}
      </StyledHitoryContaner>
    </StyledBoardWraper>
  );
};
