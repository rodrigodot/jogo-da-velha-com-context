module.exports = {
  trailingComma: 'es5',
  endOfLine: 'auto',
  singleQuote: true,
  printWidth: 70,
};
