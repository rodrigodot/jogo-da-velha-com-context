import React, { FC } from 'react';

import { ButtonProtocol } from './button.types';
import { StyledButton } from './button.styles';

export const Button: FC<ButtonProtocol> = ({
  title,
  onClick,
  buttonStyles,
  buttonProps,
}: ButtonProtocol) => {
  return (
    <StyledButton
      {...buttonProps}
      style={buttonStyles}
      onClick={() => onClick()}
      color={buttonStyles ? buttonStyles.color : '#0080ff'}
    >
      {title}
    </StyledButton>
  );
};
