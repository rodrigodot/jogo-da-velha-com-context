import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useState,
} from 'react';

export type GameContextType = {
  currentPlayer?: string;
  isWinner?: string;
  history?: Array<string>;
  squares?: Array<string>;
  reset?: () => void;
  setCurrentPlayer?: Dispatch<SetStateAction<string>>;
  setIsWinner?: Dispatch<SetStateAction<string>>;
  setHistory?: Dispatch<SetStateAction<Array<string>>>;
};

export const GameContext = createContext<GameContextType>({});

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const GameContextProvider = (props: {
  children: React.ReactChild;
}) => {
  const [currentPlayer, setCurrentPlayer] = useState<string>('X');
  const [squares, setSquares] = useState<Array<string>>(
    Array(9).fill('')
  );
  const [isWinner, setIsWinner] = useState<string>('');
  const [history, setHistory] = useState<Array<string>>([]);

  const reset = () => {
    setCurrentPlayer('X');
    setSquares(Array(9).fill(''));
    setIsWinner('');
    setHistory([]);
  };

  const context = {
    squares,
    reset,
    currentPlayer,
    setCurrentPlayer,
    isWinner,
    setIsWinner,
    history,
    setHistory,
  };

  return (
    <GameContext.Provider value={context}>
      {props.children}
    </GameContext.Provider>
  );
};
