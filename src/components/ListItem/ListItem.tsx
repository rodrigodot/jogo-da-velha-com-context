import React, { FC } from 'react';

import { ListItemProtocol } from './listItem.types';
import { StyledWraper } from './listItem.styles';

export const ListItem: FC<ListItemProtocol> = ({
  index,
  details,
}: ListItemProtocol) => {
  return <StyledWraper>{`${index} - ${details}`}</StyledWraper>;
};
